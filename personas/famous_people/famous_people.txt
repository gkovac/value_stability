Marilyn Monroe (1926 – 1962) American actress, singer, model.
Abraham Lincoln (1809 – 1865) US President during American civil war.
Nelson Mandela (1918 – 2013)  South African President anti-apartheid campaigner.
Queen Elizabeth II (1926 – 2022) British monarch since 1954.
John F. Kennedy (1917 – 1963) US President 1961 – 1963.
Martin Luther King (1929 – 1968)  American civil rights campaigner.
Winston Churchill (1874 – 1965) British Prime Minister during WWII.
Donald Trump (1946 – ) Businessman, US President.
Bill Gates (1955 – ) American businessman, founder of Microsoft.
Muhammad Ali (1942 – 2016) American Boxer and civil rights campaigner.
Mahatma Gandhi (1869 – 1948) Leader of Indian independence movement.
Mother Teresa (1910 – 1997) Macedonian Catholic missionary nun.
Christopher Columbus (1451 – 1506) Italian explorer.
Charles Darwin (1809 – 1882) British scientist, theory of evolution.
Elvis Presley (1935 – 1977) American musician.
Albert Einstein (1879 – 1955) German scientist, theory of relativity.
Paul McCartney (1942 – ) British musician, member of Beatles.
Queen Victoria ( 1819 – 1901) British monarch 1837 – 1901.
Pope Francis (1936 – ) First pope from the Americas.
Jawaharlal Nehru (1889 – 1964) Indian Prime Minister 1947 – 1964.
Leonardo da Vinci (1452 – 1519) Italian, painter, scientist, polymath.
Vincent Van Gogh (1853 – 1890) Dutch artist.
Franklin D. Roosevelt (1882 – 1945) US President 1932 – 1945.
Pope John Paul II (1920 – 2005) Polish Pope.
Thomas Edison ( 1847 – 1931) American inventor.
Rosa Parks (1913 – 2005)  American civil rights activist.
Lyndon Johnson (1908 – 1973) US President 1963 – 1969.
Ludwig Beethoven (1770 – 1827) German composer.
Oprah Winfrey (1954 – ) American TV presenter, actress, entrepreneur.
Indira Gandhi (1917 – 1984) Prime Minister of India 1966 – 1977.
Eva Peron (1919 – 1952) First Lady of Argentina 1946 – 1952.
Benazir Bhutto (1953 – 2007) Prime Minister of Pakistan 1993 – 1996.
George Orwell (1903 – 1950) British author.
Vladimir Putin (1952 – ) Russian leader.
Dalai Lama (1938 – ) Spiritual and political leader of Tibetans.
Walt Disney (1901 – 1966) American film producer.
Neil Armstrong (1930 – 2012) US astronaut.
Peter Sellers (1925 – 1980) British actor and comedian.
Barack Obama (1961 – ) US President 2008 – 2016.
Malcolm X (1925 – 1965) American Black nationalist leader.
J.K.Rowling (1965 – ) British author.
Richard Branson (1950 – ) British entrepreneur.
Pele (1940 – ) Brazilian footballer, considered greatest of 20th century.
Angelina Jolie (1975 – ) Actress, director, humanitarian.
Jesse Owens (1913 – 1980) US track athlete, 1936 Olympics.
John Lennon (1940 – 1980) British musician, member of the Beatles.
Henry Ford (1863 – 1947) US Industrialist.
Haile Selassie (1892 – 1975) Emperor of Ethiopia 1930 – 1974.
Joseph Stalin (1879 – 1953) Leader of Soviet Union 1924 – 1953.
Lord Baden Powell (1857 – 1941) British Founder of scout movement.